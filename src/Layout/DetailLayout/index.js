import { Layout } from "antd";
import { Outlet } from "react-router-dom";
const DetailLayout = () => {
  return (
    <>
      <Layout>
        <Outlet />
      </Layout>
    </>
  );
};

export default DetailLayout;

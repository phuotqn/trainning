import styled from "styled-components";

const PrivateLayoutWrapper = styled.div`
  .ant-layout {
    min-height: 100vh;
    .ant-layout-sider {
      display: flex;
      justify-content: center;
      border: 1px solid var(--border-color);
      flex-direction: column;
      background-color: var(--bg-color);
      .inputContainer {
        max-width: 290px;
        padding-top: 10px;
        padding-left: 16px;
        min-width: 46px;
        margin-right: 20px;
        margin-left: 1px;
        display: flex;
        justify-content: center;
        align-items: center;
        .ant-input-affix-wrapper {
          gap: 5px;
          min-width: 46px;
          height: 36px;
          max-width: 290px;
          font-size: 12px;
          color: var(--input-icolor);
          font-weight: 500;
        }
      }

      .userInfo {
        position: absolute;
        bottom: 0;
        display: flex;
        padding: 0 16px;
        justify-content: space-between;
        border-top: 1px solid var(--border-color);
        align-items: center;
        width: 100%;
        height: 60px;
        max-height: 60px;
      }
      .demo-logo-vertical {
        width: 115.883px;
        max-width: 90px;
        min-width: 60%;
        padding: 10px;
        position: relative;
      }
      .ant-menu {
        padding: 10px;
        .ant-menu-item {
          color: var(--item-menu);
        }
        .ant-menu-item-selected {
          align-items: center;
          color: var(--primary-color);
          .indicator {
            position: absolute;
            width: 3px;
            min-width: 3px;
            max-width: 3px;
            height: 24px;
            border: 3px solid var(--primary-color);
            left: 10px;
            top: 8px;
            border-radius: 5px;
          }
          .ant-icon {
          }
          svg {
            display: flex;
            justify-content: center;
            align-items: center;
          }
        }
      }
      .trigger {
        display: flex;
        justify-content: center;
        align-items: center;
        position: absolute;
        top: 40px;
        right: -10px;
        width: 20px;
        height: 20px;
        border-radius: 3px;
        border: 2px solid var(--border-color);
        color: var(--primary-color);
        background: var(--bg-color);
      }
    }
  }
  .ant-layout-header {
    background-color: var(--bg-color);
    display: flex;
    align-items: center;
    font-size: 16px;
    font-weight: 600;
    justify-content: space-between;
    padding-left: 20px;
    .title {
      display: flex;
      justify-content: center;
      gap: 10px;
      align-items: center;
      .btnBack {
        width: 60px;
        display: flex;
        height: 26px;
        align-items: center;
        justify-content: center;
        border-radius: 5px;
        background-color: rgba(68, 39, 227, 0.05);
        font-size: 11px;
        font-weight: bold;
        font-style: normal;
        color: var(--primary-color);
      }
    }
    .ant-input {
      gap: 10px;
      width: 250px;
      height: 20px;
      border-radius: 5px;
      padding: 10px;
    }
    .buttonContainer {
      width: 70px;
      height: 29px;
      display: flex;
      align-items: center;
      justify-content: center;
      .ant-btn {
        width: "100%";
        background-color: var(--primary-color);
        display: "flex";
        height: "30px";
        justify-content: "center";
        align-items: "center";
        color: var(--bg-color);
        border-radius: "7px";
        font-size: "11px";
        font-weight: 500;
      }
      .btnCancel {
        margin-right: 5px;
        border-radius: 7px;
        background-color: rgba(104, 82, 211, 0.1);
        display: flex;
        justify-content: center;
        width: 57px;
        color: var(--primary-color);
        font-style: normal;
        font-weight: bold;
      }
      .btnSave {
        padding: 5px 10px;
        display: flex;
        justify-content: center;
        align-items: center;
        font-weight: bold;
        width: 76px;
        height: 38px;
        .anticon {
          font-size: 18px;
        }
      }
    }
  }
  .ant-table-wrapper {
    padding-top: 20px;
    display: flex;
    justify-content: center;
    align-items: center;

    .ant-table {
      width: 1200px;
      border: none;
    }
    .editable-row .ant-form-item-explain {
      position: absolute;
      top: 100%;
      font-size: 12px;
    }
    .ant-table-thead {
      font-size: 14px;
      font-weight: 600;
      width: 36px;
      height: 14px;
      .ant-table-cell {
        color: var(--input-icolor);
      }
    }
    .ant-table-row {
      font-size: 13px;
      color: #1c1c1c;
      .ant-tag-success {
        display: inline-flex;
        align-items: center;
        font-weight: 500;
        border-radius: 3px;
        background: rgba(62, 187, 19, 0.16);
        color: var(--success-tag);
        width: 50px;
        height: 20px;
      }
      .ant-tag-default {
        display: inline-flex;
        align-items: center;
        font-weight: 500;
        border-radius: 3px;
        background: rgba(87, 88, 92, 0.1);
        color: var(--inactive-tag);
        width: 60px;
        height: 20px;
      }
    }
  }
`;

export default PrivateLayoutWrapper;

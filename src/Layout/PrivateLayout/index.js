import { Layout } from "antd";
import PrivateLayoutWrapper from "./style";
import Sidebar from "../../Container/Sidebar/index";
import { Outlet } from "react-router-dom";
const PrivateLayout = () => {
  return (
    <PrivateLayoutWrapper>
      <Layout>
        <Sidebar />
        <Layout>
          <Outlet />
        </Layout>
      </Layout>
    </PrivateLayoutWrapper>
  );
};

export default PrivateLayout;

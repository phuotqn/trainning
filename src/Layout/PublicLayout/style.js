import styled from "styled-components";

const PublicLayoutWrapper = styled.div`
  .layout {
    height: 100vh;
    background: var(--main-gradient);
    flex-direction: row;
    background-repeat: repeat;
    align-items: center;
    justify-content: center;
    .contentWrapper {
      height: 350px;
      width: 400px;
      background-color: var(--bg-color);
      border-radius: 20px;
      padding: 44px 32px;
      box-shadow: 0px 4px 24px 0px rgba(0, 0, 0, 0.15);
      flex-direction: column;
      display: flex;
      justify-content: center;
      align-items: center;
      .logo {
        padding-left: 20px;
        margin-bottom: 15px;
        width: 80%;
      }
      .mainContent {
        max-width: 100%;
        min-width: 100%;
        width: 100%;
        display: flex;
        justify-content: center;
        .ant-form-item {
          width: 286px;
          height: 40px;
          margin-bottom: 20px;
          .loginInput {
            height: 38px;
            border-radius: 5px;
            border: solid 1px var(--border-color);
            color: var(--input-icolor);
            font-size: 14px;
            font-weight: 400;
          }
          .loginBtn {
            width: 286px;
            height: 42px;
            padding: 8px 16px;
            justify-content: center;
            align-items: center;
            display: flex;
            gap: 8px;
            border-radius: 5px;
            margin-top: 10px;
            background: var(--primary-color);
            color: var(--bg-color);
            font-weight: 600;
            font-size: 14px;
          }
        }
      }
    }
  }
`;

export default PublicLayoutWrapper;

import PublicLayoutWrapper from "./style";
import Logo from "../../assets/Logo";
import { Layout } from "antd";
import { Outlet } from "react-router-dom";
const PublicLayout = ({ children }) => {
  return (
    <PublicLayoutWrapper>
      <Layout className="layout">
        <div className="contentWrapper">
          <div className="logo">
            <Logo />
          </div>
          <div className="mainContent">{children}</div>
        </div>
      </Layout>
    </PublicLayoutWrapper>
  );
};

export default PublicLayout;

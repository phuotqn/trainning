import Login from "../../Container/Login/index";
import PublicLayout from "../../Layout/PublicLayout";

const LoginPage = () => {
  return (
    <PublicLayout>
      <Login />
    </PublicLayout>
  );
};

export default LoginPage;

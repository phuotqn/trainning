import HeaderBar from "../../Container/Header";
import Project from "../../Container/List";
import { PlusOutlined } from "@ant-design/icons";
import Layout from "antd/es/layout/layout";
import { useNavigate } from "react-router-dom";
const ProjectPage = () => {
  const navigate = useNavigate();
  const onCreateClick = () => {
    navigate("/projects/create");
  };
  const buttons = [
    {
      label: "New",
      Icon: <PlusOutlined />,
      onClick: onCreateClick,
    },
  ];
  return (
    <Layout>
      <HeaderBar search title="Project List" buttons={buttons} />
      <Project />
    </Layout>
  );
};

export default ProjectPage;

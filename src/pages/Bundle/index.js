import HeaderBar from "../../Container/Header";
import Bundle from "../../Container/Bundle";
import { PlusOutlined } from "@ant-design/icons";
const BundlePage = () => {
  const buttons = [
    {
      label: "New",
      Icon: <PlusOutlined />,
    },
  ];

  return (
    <>
      <HeaderBar search title="Bundle" buttons={buttons} />
      <Bundle />
    </>
  );
};

export default BundlePage;

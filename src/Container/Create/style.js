import styled from "styled-components";

const CreateFormWrapper = styled.div`
  .formWrapper {
    padding: 20px;
    .form {
      padding: 5px;
      padding-left: 20px;
      color: var(--menu-color);
      font-size: 14px;
      font-weight: 590;
      font-style: normal;
    }
    .ant-input {
      width: 237px;
      height: 35px;
    }
    .ant-select {
      width: 237px;
      border-radius: 5px;
      height: 35px;
    }
    .uploadIcon {
      font-size: 29px;
      font-weight: 900;
      color: var(--primary-color);
    }
  }
`;

export default CreateFormWrapper;

import {
  Form,
  Input,
  Row,
  Col,
  Card,
  Layout,
  Radio,
  Select,
  Upload,
} from "antd";
import { useEffect } from "react";
import HeaderBar from "../Header";
import CreateFormWrapper from "./style";
import { SaveOutlined, UploadOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import { getInstances } from "../../@redux/instances/action";
import { createProject } from "../../@redux/project/action";
const VALIDATE = {
  projectName: [
    {
      required: true,
      message: "Please provide Project name",
    },
  ],
  accessKey: [
    {
      required: true,
      message: "Please provide Access Key",
    },
  ],
  secretKey: [
    {
      required: true,
      message: "Please provide Secret Key",
    },
  ],
  gitPersonalToken: [
    {
      required: true,
      message: "Please provide Git personal token",
    },
  ],
  region: [
    {
      required: true,
      message: "Please select",
    },
  ],
};
const CreateForm = () => {
  const [form] = Form.useForm();
  const buttons = [
    {
      label: "Cancel",
      className: "btnCancel",
    },
    {
      label: "Save",
      className: "btnSave",
      Icon: <SaveOutlined />,
      onClick: () => {
        form.submit();
      },
    },
  ];
  const dispatch = useDispatch();
  const instances = useSelector((state) => state.instances.regions);
  useEffect(() => {
    dispatch(getInstances());
  }, []);
  const onSave = async (values) => {
    const response = await dispatch(createProject(values));
    if (response) {
      form.resetFields();
    }
  };
  return (
    <CreateFormWrapper>
      <HeaderBar display title="Project List / Create" buttons={buttons} />
      <Layout className="formWrapper">
        <Card title="General Infomation">
          <Form
            className="form"
            form={form}
            layout="vertical"
            onFinish={onSave}
          >
            <Row>
              <Col span={20}>
                <Form.Item
                  rules={VALIDATE.projectName}
                  name="projectName"
                  label="Project name"
                >
                  <Input />
                </Form.Item>
                <Form.Item label="Status">
                  <Radio.Group>
                    <Radio value="a">Active</Radio>
                    <Radio value="b">Inactive</Radio>
                  </Radio.Group>
                </Form.Item>
                <Form.Item
                  rules={VALIDATE.accessKey}
                  name="accessKey"
                  label="Access key"
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  rules={VALIDATE.gitPersonalToken}
                  name="gitPersonalToken"
                  label="Git personal token"
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  rules={VALIDATE.secretKey}
                  name="secretKey"
                  label="Secret key"
                >
                  <Input />
                </Form.Item>
                <Form.Item rules={VALIDATE.region} name="region" label="Region">
                  <Select>
                    {instances.map((value) => {
                      return (
                        <Select.Option value={value.code} key={value.code}>
                          {value.continentCode} {`(${value.displayName})`}{" "}
                          {`(${value.code})`}
                        </Select.Option>
                      );
                    })}
                  </Select>
                </Form.Item>
              </Col>
              <Col className="right-side" span={4}>
                <Form.Item label="Image" valuePropName="fileList">
                  <Upload action="/upload.do" listType="picture-card">
                    <div>
                      <UploadOutlined className="uploadIcon" />
                    </div>
                  </Upload>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Card>
      </Layout>
    </CreateFormWrapper>
  );
};

export default CreateForm;

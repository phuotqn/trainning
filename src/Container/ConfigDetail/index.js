import HeaderBar from "../Header";
import Wrapper from "./style";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import {
  SaveOutlined,
  PlusOutlined,
  CloseOutlined,
  CheckOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import {
  Card,
  Form,
  Row,
  Col,
  Input,
  InputNumber,
  Select,
  Radio,
  Button,
  Pagination,
  Modal,
  Popconfirm,
} from "antd";
import { SearchOutlined } from "@ant-design/icons";
import { getInstances, getTotal } from "../../@redux/instances/selector";
import {
  createInstance,
  deleteInstance,
  getConfigDetail,
  getConfigInstances,
} from "../../@redux/instances/action";

const { Option } = Select;
const buttons = [
  {
    label: "Cancel",
    className: "btnCancel",
  },
  {
    label: "Save",
    className: "btnSave",
    Icon: <SaveOutlined />,
  },
];
const tabLists = [
  {
    key: "instance",
    label: "Instance",
  },
  {
    key: "bundles",
    label: "Bundle",
  },
];
const listTitle = [
  "Pricing",
  "Supported Platforms",
  "CPU Count",
  "Disk (GB)",
  "Power",
  "RAM (GB)",
  "Monthly transfer (GB)",
];
const popConfirm = {
  title: "Delete instance",
  description: "Delete this instance ?",
  okText: "Yes",
  cancelText: "No",
};
const pageSize = 4;
const cardDetail = ["Public IP", "Private IP", "CPU", "Type", "RAM"];
const ConfigDetail = () => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { id } = useParams();
  const configInstances = useSelector(getInstances);
  const bundles = Object.values(useSelector((state) => state.bundles.data));
  const total = useSelector(getTotal);
  const configDetail = useSelector((state) => state.instances.configDetail);
  const [activeTab, setActiveTab] = useState("instance");
  const [checked, setChecked] = useState(null);
  const [select, setSelect] = useState(null);
  const [amount, setAmount] = useState(0);
  const onTabChange = (key) => {
    setActiveTab(key);
  };
  const handleClick = (e) => {
    setChecked(e);
  };
  const [currentPage, setCurrentPage] = useState(1);
  const [open, setOpen] = useState(false);
  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  const onChangeAmount = (e) => {
    if (e === 0) {
      setAmount(1);
    } else {
      setAmount(e);
    }
  };
  const handleOk = () => {
    if (select === "default") {
      setAmount(1);
    }
    const body = {
      configId: id,
      amount: amount,
    };
    dispatch(createInstance(body));
  };
  const handleCancel = () => {
    setOpen(false);
  };
  const onTypeSelect = (e) => {
    setSelect(e.target.value);
  };
  const onConfirmDelete = (e) => {
    const body = {
      instanceId: e,
    };
    dispatch(deleteInstance(body));
  };
  useEffect(() => {
    dispatch(getConfigDetail(id));
    dispatch(
      getConfigInstances({ page: currentPage, limit: pageSize, configId: id })
    );
  }, [currentPage, dispatch, total]);
  const contentList = {
    instance: (
      <Row className="cardWrapper" gutter={16}>
        <Col span={4}>
          <Card className="cardAdd">
            <Button
              className="btnAdd"
              type="dashed"
              icon={<PlusOutlined className="icon" />}
              onClick={() => setOpen(true)}
            />
          </Card>
        </Col>
        {configInstances &&
          configInstances.map((config) => {
            return (
              <Col span={4}>
                <Card className="infoWrapper">
                  <Row>
                    <Popconfirm
                      key={config.id}
                      title={popConfirm.text}
                      description={popConfirm.description}
                      okText={popConfirm.okText}
                      cancelText={popConfirm.cancelText}
                      icon={
                        <QuestionCircleOutlined
                          style={{ marginTop: "7px", color: "red" }}
                        />
                      }
                      onConfirm={() => onConfirmDelete(config.id)}
                    >
                      <Button
                        className="deleteBtn"
                        icon={<CloseOutlined className="deleteIcon" />}
                      />
                    </Popconfirm>

                    <Col className="left" span={9}>
                      {cardDetail.map((item) => {
                        return <span>{item}</span>;
                      })}
                    </Col>
                    <Col className="right" span={9}>
                      <span>123.26.105.66</span>
                      <span>123.26.105.66</span>
                      <span>2</span>
                      <span>NANO</span>
                      <span>8GB</span>
                    </Col>
                  </Row>
                </Card>
              </Col>
            );
          })}

        <Row className="pagination">
          <Pagination
            total={total}
            current={currentPage}
            pageSize={pageSize}
            onChange={handlePageChange}
            showTotal={(total) => `Total ${total} items`}
          />
        </Row>
      </Row>
    ),
    bundles: (
      <Card className="bundleSection">
        <Row className="titleWrapper">
          {listTitle.map((list) => {
            return (
              <Col className="listTitle" span={3}>
                {list}
              </Col>
            );
          })}
        </Row>
        {bundles.map((item, key) => {
          return (
            <Form
              key={item.id}
              className={
                checked === item.id ? "bundleItemActive" : "bundleItem"
              }
              form={form}
            >
              <Col span={2} className="nameCol">
                <Form.Item key={item.id} name="bundleId"></Form.Item>
                <p className="name">{item.name}</p>
              </Col>
              <Col className="pricingCol" span={2}>
                <p>${item.price}</p>
              </Col>
              <Col className="supportCol" span={3}>
                <p>{item.support}</p>
              </Col>
              <Col className="cpuCol" span={3}>
                <p>{item.cpu}</p>
              </Col>
              <Col className="diskCol" span={3}>
                <p>{item.code}</p>
              </Col>
              <Col className="powerCol" span={3}>
                <p>{item.power}</p>
              </Col>
              <Col className="rightEnd" span={3}>
                <p>{item.ram}</p>
              </Col>
              <Col className="rightEnd" span={3}>
                <p>{item.monthlyTransfer}</p>
              </Col>
              <Col className="action">
                <Button
                  key={item.id}
                  onClick={() => handleClick(item.id)}
                  className={
                    checked === item.id ? "selectedButton" : "selectButton"
                  }
                >
                  {checked === item.id ? <CheckOutlined /> : "Select"}
                </Button>
              </Col>
            </Form>
          );
        })}
      </Card>
    ),
  };
  return (
    <>
      <HeaderBar
        display
        title={`${configDetail?.name} / Config Detail`}
        buttons={buttons}
      />
      <Wrapper>
        <Card title="General information">
          <Form form={form} layout="vertical" className="formWrapper">
            {configDetail && (
              <Row>
                <Col span={9} className="leftCol">
                  <Form.Item name="name" label="Name">
                    <Input defaultValue={configDetail.name} className="input" />
                  </Form.Item>
                  <Form.Item name="availabilityZone" label="Availability zone">
                    <Select
                      defaultValue={configDetail.availabilityZone}
                      className="input"
                    />
                  </Form.Item>
                  <Form.Item name="name" label="Min instance">
                    <InputNumber
                      defaultValue={configDetail.minInstant}
                      className="input"
                    />
                  </Form.Item>
                  <Form.Item name="name" label="SSH key">
                    <Input
                      defaultValue={configDetail.sshKey}
                      className="input"
                    />
                  </Form.Item>
                </Col>
                <Col span={8} className="midCol">
                  <Form.Item
                    label="Status"
                    className="radioGroup"
                    name="isActive"
                  >
                    <Radio.Group defaultValue={true}>
                      <Radio value={true}>Active</Radio>
                      <Radio value={false}>Inactive</Radio>
                    </Radio.Group>
                  </Form.Item>
                  <Form.Item name="privatePort" label="Private port">
                    <Input
                      defaultValue={configDetail.privatePort}
                      className="input"
                    />
                  </Form.Item>
                  <Form.Item name="loadBalanceId" label="Load balance">
                    <Select className="input" />
                  </Form.Item>
                </Col>
                <Col span={7} className="rightCol">
                  <Form.Item label="Git project" name="gitProjectId">
                    <Select
                      className="input"
                      showSearch
                      suffixIcon={<SearchOutlined />}
                    >
                      <Option></Option>
                    </Select>
                  </Form.Item>
                  <Form.Item label="Git branch" name="gitBranchName">
                    <Select
                      className="input"
                      showSearch
                      suffixIcon={<SearchOutlined />}
                    >
                      <Option></Option>
                    </Select>
                  </Form.Item>
                  <Form.Item label="Git variable" name="gitVariableEnvFile">
                    <Select className="input">
                      <Option></Option>
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
            )}
          </Form>
        </Card>
        <Card className="tabList" tabList={tabLists} onTabChange={onTabChange}>
          {contentList[activeTab]}
        </Card>
        <Modal
          title="New instance"
          open={open}
          onCancel={handleCancel}
          footer={[
            <Button className="btnCancel" onClick={handleCancel}>
              Cancel
            </Button>,
            <Button
              onClick={handleOk}
              className="btnSave"
              icon={<SaveOutlined />}
            >
              Save
            </Button>,
          ]}
        >
          <Row className="contentWrapper">
            <Col span={2}>
              <Radio.Group onChange={onTypeSelect} defaultValue={"default"}>
                <Radio className="defaultRad" value="default" />
                <Radio className="customRad" value="custom" />
              </Radio.Group>
            </Col>
            <Col span={4}>
              <p>Default</p>
              <p>Custom</p>
            </Col>
            <Col className="inputRow" span={12}>
              {bundles && (
                <Input
                  disabled
                  value={`${bundles[0].name} CPU ${bundles[0].cpu} ${bundles[0].ram}GB`}
                ></Input>
              )}
              {select === "custom" ? (
                <Select onChange={onChangeAmount}>
                  {bundles.map((bundle, index) => {
                    return (
                      <Option key={bundle.id} value={index}>
                        {`${bundle.name} CPU ${bundle.cpu} ${bundle.ram}GB`}{" "}
                      </Option>
                    );
                  })}
                </Select>
              ) : (
                <Select disabled></Select>
              )}
            </Col>
          </Row>
        </Modal>
      </Wrapper>
    </>
  );
};

export default ConfigDetail;

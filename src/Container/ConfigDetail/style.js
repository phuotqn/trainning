import styled from "styled-components";

const Wrapper = styled.div`
  padding: 16px;
  .formWrapper {
    padding-top: 8px;
    label {
      color: var(--input-icolor);
      font-size: 14px;
      font-weight: 600;
    }
    .leftCol {
      padding-left: 65px;
      display: flex;
      justify-content: start;
      flex-direction: column;
      .input {
        width: 235px;
        height: 35px;
        border-radius: 10px;
        background-color: var(--border-color);
      }
    }
    .midCol {
      display: flex;
      align-items: start;
      flex-direction: column;
      .input {
        width: 235px;
        height: 35px;
        border-radius: 10px;
        background-color: var(--border-color);
      }
    }
    .rightCol {
      display: flex;
      align-items: start;
      flex-direction: column;
      .input {
        width: 235px;
        height: 35px;
        border-radius: 10px;
        background-color: var(--border-color);
      }
    }
  }
  .tabList {
    margin-top: 16px;
    position: relative;
    .ant-tabs-tab-btn {
      font-size: 14px;
      font-weight: 590;
      color: var(--inactive-tab);
    }
    .cardWrapper {
      position: relative;
      height: 250px;
      display: flex;
      flex-direction: row;
      justify-content: center;
      gap: 40px;
      .pagination {
        position: absolute;
        bottom: 0;
        right: 0;
        display: flex;
        justify-content: end;
        align-items: end;
      }
    }
    .cardAdd {
      display: flex;
      justify-content: center;
      align-items: center;
      width: 230px;
      height: 160px;
      border-radius: 10px;
      border: 2px solid var(--border-color);
      position: relative;
      .btnAdd {
        width: 80px;
        height: 80px;
        border-radius: 50%;
        border-width: 3px;
        border-color: var(--primary-color);
        display: flex;
        justify-content: center;
        align-items: center;
        .icon {
          width: 28px;
          height: 32px;
          font-size: 32px;
          font-weight: 900;
          color: var(--primary-color);
        }
      }
    }
    .infoWrapper {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      width: 230px;
      height: 160px;
      border-radius: 10px;
      position: relative;
      border: 2px solid var(--border-color);
      .deleteBtn {
        position: absolute;
        top: 0;
        right: 0;
        transform: translateX(14px) translateY(-12px);
        width: 30px;
        height: 30px;
        border-radius: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        object-fit: contain;
        background-color: var(--primary-color);
        .deleteIcon {
          color: white;
          font-size: 14px;
          font-weight: 900;
        }
      }
      .left {
        display: flex;
        flex-direction: column;
        color: var(--menu-color);
        justify-content: center;
        font-size: 14px;
        font-weight: 600;
      }
      .right {
        display: flex;
        align-items: start;
        margin-left: 30px;
        justify-content: center;
        flex-direction: column;
        font-size: 14px;
        font-weight: 390;
      }
    }
    .bundleSection {
      border: none;
      color: var(--menu-color);
      font-size: 14px;
      font-style: normal;
      font-weight: 600;
      display: flex;
      flex-direction: column;
      justify-content: center;
      .radio {
        margin-top: 23px;
      }
      .titleWrapper {
        display: flex;
        justify-content: center;
        .listTitle:nth-child(1) {
          display: flex;
          justify-content: center;
          align-items: end;
        }
      }
      .bundleItem {
        padding: 10px;
        display: flex;
        margin: 20px 0px;
        border: 2px solid var(--border-color);
        border-radius: 10px;
        align-items: center;
        justify-content: center;
        p {
          color: var(--menu-color);
          font-size: 14px;
          font-weight: 400;
        }
        .nameCol {
          display: flex;
          gap: 10px;
          align-items: center;
          .name {
            font-size: 14px;
            font-style: normal;
            font-weight: 700;
            color: var(--primary-color);
          }
        }
        .pricingCol {
          padding-left: 24px;
        }
        .supportCol {
          padding-left: 24px;
        }
        .cpuCol {
          padding-left: 30px;
        }
        .diskCol {
          padding-left: 30px;
        }
        .powerCol {
          padding-left: 30px;
        }
        .rightEnd {
          padding-left: 35px;
        }
        .action {
          display: flex;
          margin-left: 35px;
          align-items: center;
        }
        .selectButton {
          display: flex;
          justify-content: center;
          width: 60px;
          height: 30px;
          align-items: center;
          background-color: rgba(104, 82, 211, 0.1);
          border-radius: 5px;
          color: var(--primary-color);
        }
        .selectedButton {
          display: flex;
          justify-content: center;
          width: 60px;
          height: 30px;
          align-items: center;
          background-color: var(--primary-color);
          border-radius: 5px;
          color: white;
        }
      }
      .ipAddress {
        padding-top: 10px;
      }
      .bundleItemActive {
        padding: 10px;
        display: flex;
        margin: 20px 0px;
        border: 2px solid var(--primary-color);
        border-radius: 10px;
        align-items: center;
        p {
          color: var(--menu-color);
          font-size: 14px;
          font-weight: 400;
        }
        .nameCol {
          display: flex;
          gap: 10px;
          align-items: center;
          .name {
            font-size: 14px;
            font-style: normal;
            font-weight: 700;
            color: var(--primary-color);
          }
        }
        .pricingCol {
          padding-left: 24px;
        }
        .supportCol {
          padding-left: 24px;
        }
        .cpuCol {
          padding-left: 30px;
        }
        .diskCol {
          padding-left: 30px;
        }
        .powerCol {
          padding-left: 30px;
        }
        .rightEnd {
          padding-left: 35px;
        }
        .action {
          display: flex;
          margin-left: 35px;
          align-items: center;
        }
        .selectButton {
          display: flex;
          justify-content: center;
          width: 60px;
          height: 30px;
          align-items: center;
          background-color: rgba(104, 82, 211, 0.1);
          border-radius: 5px;
          color: var(--primary-color);
        }
        .selectedButton {
          display: flex;
          justify-content: center;
          width: 60px;
          height: 30px;
          align-items: center;
          background-color: var(--primary-color);
          border-radius: 5px;
          color: white;
        }
      }
    }
  }
`;

export default Wrapper;

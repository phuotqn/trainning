import { Layout, Menu, Avatar, Input } from "antd";
import { useState } from "react";
import { Link, useNavigate, useLocation } from "react-router-dom";
import {
  HomeOutlined,
  AppstoreOutlined,
  DoubleLeftOutlined,
  DoubleRightOutlined,
  CreditCardFilled,
  UserOutlined,
  CaretUpOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import Logo from "../../assets/Logo";
import LogoutIcon from "../../assets/LogoutIcon";

const { Sider } = Layout;
const getCurrentTab = (str, key) => {
  const paths = str && str.split("/");
  return paths && paths[key];
};

const items = [
  {
    key: "metrix",
    text: "Metrix",
    Icon: HomeOutlined,
    url: "metrix",
  },
  {
    key: "bundle",
    text: "Bundle",
    Icon: CreditCardFilled,
    url: "bundle",
  },
  {
    key: "projects",
    text: "Projects",
    Icon: AppstoreOutlined,
    url: "projects/list",
  },
];

const Sidebar = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const [collapsed, setCollapsed] = useState(false);
  const url = getCurrentTab(location.pathname, 1);
  function trigger() {
    setCollapsed(!collapsed);
  }

  return (
    <Sider
      collapsible
      collapsed={collapsed}
      onCollapse={(value) => setCollapsed(value)}
      width={300}
      trigger={null}
    >
      <div className="demo-logo-vertical">
        <Logo />
      </div>
      <div className="inputContainer">
        <Input placeholder="Search order" prefix={<SearchOutlined />} />
      </div>
      <Menu defaultSelectedKeys={["bundle"]} mode="inline">
        <span className="trigger" onClick={trigger}>
          {collapsed ? (
            <DoubleRightOutlined style={{ fontSize: "14px" }} />
          ) : (
            <DoubleLeftOutlined style={{ fontSize: "14px" }} />
          )}
        </span>
        {items.map((item) => {
          return (
            <Menu.Item
              onClick={() => {
                navigate(item.url);
              }}
              key={item.key}
              icon={<item.Icon />}
            >
              <div className="indicator" />
              <div>{item.text}</div>
            </Menu.Item>
          );
        })}
      </Menu>
      <div className="userInfo">
        <div>
          <span>
            <Avatar size="medium" icon={<UserOutlined />} />
          </span>
          {}
          <span
            style={{
              color: "#CCD2DA",
              fontSize: "11px",
            }}
          >
            {" "}
            <CaretUpOutlined />
            {/* {!collapsed ? `${user.email}` : null} */}
          </span>
        </div>
        {!collapsed ? (
          <span>
            <LogoutIcon />
          </span>
        ) : null}
      </div>
    </Sider>
  );
};

export default Sidebar;

import styled from "styled-components";

const ProjectWrapper = styled.div`
  padding: 20px;
  .title {
    background-color: var(--bg-color);
    border-radius: 5px;
    .ant-collapse-content-box {
      padding: 0px;
    }
    .container {
      border: 1px solid var(--item-menu);
    }
    .title-text {
      font-size: 14px;
      line-height: normal;
      font-weight: bold;
      color: var(--title-color);
      padding-left: 8px;
    }
  }
  .project-info {
    border-radius: 5px;
    background-color: var(--bg-color);
    .ant-table-thead .ant-table-cell {
      font-size: 12px;
      font-style: normal;
      font-weight: 600;
      line-height: normal;
      text-transform: uppercase;
      color: var(--input-icolor);
      background-color: var(--bg-color);
      border: 0px;
      border-bottom: 1px solid var(--border-color);
    }
    .ant-btn {
      border: 0px;
    }
    .product-img {
      width: 40px;
      height: 40px;
    }
    .name {
      color: var(--primary-color);
    }
  }
  .ant-table-wrapper
    .ant-table-thead
    > tr
    > th:not(:last-child):not(.ant-table-selection-column):not(
      .ant-table-row-expand-icon-cell
    ):not([colspan])::before {
    width: 0px;
    content: none;
  }
  .ant-table-wrapper
    .ant-table-thead
    > tr
    > td:not(:last-child):not(.ant-table-selection-column):not(
      .ant-table-row-expand-icon-cell
    ):not([colspan])::before {
    content: "none";
    width: 0px;
  }
  .ant-table-tbody .ant-table-cell {
    font-size: 13px;
    font-weight: 400;
    line-height: normal;
    color: var(--dashboard-color);
    border: 0px;
  }
  .btnDelete {
    color: red;
  }
`;

export default ProjectWrapper;

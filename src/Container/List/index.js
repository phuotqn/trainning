import { Table, Space, Tag, Button, Typography, Popconfirm } from "antd";
import ProjectWrapper from "./style";
import { useDispatch, useSelector } from "react-redux";
import { getProjectLists, deleteProject } from "../../@redux/project/action";
import { DeleteOutlined } from "@ant-design/icons";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { getProjectList } from "../../@redux/project/selector";
const Project = () => {
  const navigate = useNavigate();
  const projectList = useSelector(getProjectList);
  const loading = useSelector((state) => state.projects.loading);
  const dispatch = useDispatch();
  const onDetailClick = (row) => {
    navigate(`${row.id}`);
  };
  // Delete project
  const onConfirmClick = (row) => {
    dispatch(deleteProject(row));
  };
  useEffect(() => {
    dispatch(getProjectLists());
  }, [dispatch]);
  const formatTime = (date) => {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? "pm" : "am";
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    var strTime = hours + ":" + minutes + " " + ampm;
    return strTime;
  };
  const columns = [
    {
      title: "name",
      dataIndex: "image",
      key: "image",
      render: (record) => {
        if (!record) {
          return <p>Img</p>;
        } else {
          return <img className="product-img" src={record} alt="" />;
        }
      },
      width: "5%",
    },
    {
      title: "",
      dataIndex: "projectName",
      key: "name",
      render: (record) => {
        return <Typography className="name">{record}</Typography>;
      },
    },
    {
      title: "status",
      render: () => {
        return <Tag color="green">Active</Tag>;
      },
      width: "5%",
    },
    {
      title: "region",
      dataIndex: "region",
      key: "region",
      width: "15%",
    },
    {
      title: "total environment",
      dataIndex: "configQuantity",
      key: "region",
      width: "20%",
    },
    {
      title: "update time",
      dataIndex: "updatedAt",
      key: "region",
      width: "20%",
      render: (record) => {
        return (
          <p>
            {new Date(record).toLocaleDateString("en-GB")}, {""}
            {formatTime(new Date(record))}
          </p>
        );
      },
    },
    {
      title: "",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button
            onClick={() => {
              onDetailClick(record);
            }}
          >
            <svg
              width="13"
              height="14"
              viewBox="0 0 13 14"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M11.959 1.18359C11.4004 0.625 10.5117 0.625 9.95312 1.18359L9.19141 1.94531L11.6797 4.43359L12.4414 3.67188C13 3.11328 13 2.22461 12.4414 1.66602L11.959 1.18359ZM4.36719 6.76953C4.21484 6.92188 4.08789 7.125 4.01172 7.32812L3.27539 9.58789C3.19922 9.79102 3.25 10.0449 3.42773 10.1973C3.58008 10.375 3.83398 10.4258 4.03711 10.3496L6.29688 9.61328C6.5 9.53711 6.70312 9.41016 6.85547 9.25781L11.0957 5.01758L8.60742 2.5293L4.36719 6.76953ZM2.4375 2.25C1.0918 2.25 0 3.3418 0 4.6875V11.1875C0 12.5332 1.0918 13.625 2.4375 13.625H8.9375C10.2832 13.625 11.375 12.5332 11.375 11.1875V8.75C11.375 8.31836 10.9941 7.9375 10.5625 7.9375C10.1055 7.9375 9.75 8.31836 9.75 8.75V11.1875C9.75 11.6445 9.36914 12 8.9375 12H2.4375C1.98047 12 1.625 11.6445 1.625 11.1875V4.6875C1.625 4.25586 1.98047 3.875 2.4375 3.875H4.875C5.30664 3.875 5.6875 3.51953 5.6875 3.0625C5.6875 2.63086 5.30664 2.25 4.875 2.25H2.4375Z"
                fill="#6852D3"
              />
            </svg>
          </Button>
          <Popconfirm
            title="Are you sure you want to delete this project?"
            onConfirm={() => onConfirmClick(record.id)}
            okText="Yes"
            cancelText="No"
          >
            <Button className="btnDelete" icon={<DeleteOutlined />} />
          </Popconfirm>
        </Space>
      ),
      width: "5%",
    },
  ];
  return (
    <ProjectWrapper>
      <Table
        className="project-info"
        bordered={false}
        dataSource={projectList}
        columns={columns}
        pagination={true}
        loading={loading}
      />
    </ProjectWrapper>
  );
};
export default Project;

import styled from "styled-components";

const TitleWrapper = styled.div`
  font-size: 16px;
  font-weight: 600;
  font-style: normal;
`;

export default TitleWrapper;

import React from "react";
import { Layout, Input, Button } from "antd";
import { SearchOutlined, LeftOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import TitleWrapper from "./style";
const { Header } = Layout;
function HeaderBar({ title, buttons, display, search }) {
  const navigate = useNavigate();
  return (
    <Header>
      <div className="title">
        <Button
          onClick={() => {
            navigate(-1);
          }}
          style={!display ? { display: "none" } : ""}
          className="btnBack"
          icon={<LeftOutlined />}
        >
          Back
        </Button>
        <TitleWrapper>{title}</TitleWrapper>
      </div>

      <div>
        {search ? (
          <Input
            placeholder="Search"
            prefix={<SearchOutlined />}
            suffix="..."
          ></Input>
        ) : null}
      </div>
      <div className="buttonContainer">
        {buttons &&
          buttons.map((button, index) => {
            return (
              <Button
                key={index}
                title={button.label}
                icon={button.Icon}
                className={button.className}
                htmlType={button.htmlType}
                onClick={button.onClick}
              >
                {button.label}
              </Button>
            );
          })}
      </div>
    </Header>
  );
}

export default HeaderBar;

import EditWrapper from "./style";
import HeaderBar from "../Header";
import { SaveOutlined, UploadOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { editProject, getProjectById } from "../../@redux/project/action";
import { useParams } from "react-router-dom";
import { Card, Layout, Row, Col, Form, Input, Radio, Upload } from "antd";
import { getProjectByIdSelector } from "../../@redux/project/selector";
const Edit = () => {
  const [form] = Form.useForm();
  const { id } = useParams();
  const dispatch = useDispatch();
  const data = useSelector((state) =>
    getProjectByIdSelector(state, { id: id })
  );
  let title = data.projectName;
  const buttons = [
    {
      label: "Cancel",
      className: "btnCancel",
    },
    {
      label: "Save",
      className: "btnSave",
      Icon: <SaveOutlined />,
      onClick: () => {
        form.submit();
      },
    },
  ];
  useEffect(() => {
    dispatch(getProjectById(id));
  }, [dispatch]);
  useEffect(() => {
    if (data) {
      form.setFieldsValue({
        projectName: data.projectName,
        apiKey: data.apiKey,
        gitPersonalToken: data.gitPersonalToken,
        region: data.region,
        accessKey: data.accessKey,
      });
    }
  }, [data, dispatch]);
  const onEdit = async (body) => {
    dispatch(editProject({ id, body }));
  };
  return (
    <>
      {data && (
        <EditWrapper>
          <HeaderBar
            display
            title={data.projectName + "/ Edit"}
            buttons={buttons}
          />
          <Layout className="wrapper">
            <Card title="General Information">
              <Form form={form} layout="vertical" onFinish={onEdit}>
                <Row>
                  <Col className="leftSection" span={10}>
                    <Form.Item label="Project Name" name="projectName">
                      <Input />
                    </Form.Item>
                    <Form.Item label="Status" className="radioGroup">
                      <Radio.Group defaultValue={"a"}>
                        <Radio value="a">Active</Radio>
                        <Radio value="b">Inactive</Radio>
                      </Radio.Group>
                    </Form.Item>
                    <Form.Item label="Api key" name="apiKey">
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col span={7}>
                    <Form.Item label="Personal token" name="gitPersonalToken">
                      <Input />
                    </Form.Item>
                    <Form.Item label="Region" name="region">
                      <Input className="region" disabled />
                    </Form.Item>
                    <Form.Item label="Access key" name="accessKey">
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col span={7}>
                    <Form.Item label="Image" valuePropName="fileList">
                      <Upload action="/upload.do" listType="picture-card">
                        <div>
                          <UploadOutlined className="uploadIcon" />
                        </div>
                      </Upload>
                    </Form.Item>
                    <Form.Item
                      className="secretKey"
                      label="Secret key"
                      name="secretKey"
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Card>

            <Card title="Load Balance" className="balanceSection">
              <Form className="balanceWrapper" layout="vertical">
                <Row>
                  <Col span={6}>
                    <Form.Item label="ID">
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col>
                    <Form.Item label="Name">
                      <Input />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col className="bottomSection" span={10}>
                    <Form.Item label="Status">
                      <Input />
                    </Form.Item>
                    <Form.Item label="IP">
                      <Input />
                    </Form.Item>
                    <Form.Item label="SSH Hey">
                      <Input />
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Card>
          </Layout>
        </EditWrapper>
      )}
    </>
  );
};

export default Edit;

import styled from "styled-components";

const EditWrapper = styled.div`
  .wrapper {
    padding: 20px;

    .ant-input {
      width: 235px;
    }
    .region {
      background-color: rgba(217, 217, 217, 0.75);
    }
    .leftSection {
      display: flex;
      align-items: center;
      flex-direction: column;
    }
    label {
      color: var(--menu-color);
      font-size: 14px;
      font-weight: 600;
    }
    .radioGroup {
      margin-right: 64px;
    }
    .uploadIcon {
      font-size: 29px;
      font-weight: 900;
      color: var(--primary-color);
    }
    .secretKey {
      padding-top: 10px;
    }
    .balanceSection {
      margin-top: 16px;
    }
    .bottomSection {
      padding-top: 20px;
      display: flex;
      flex-direction: column;
      align-items: center;
    }
  }
`;

export default EditWrapper;

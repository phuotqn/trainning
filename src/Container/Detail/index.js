import React from "react";
import HeaderBar from "../Header";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";
import { getProjectById } from "../../@redux/project/action";
import { Space, Card, Table, Tag, Button, Typography, Row, Col } from "antd";
import DetailWrapper from "./style";
import { useDispatch, useSelector } from "react-redux";
import { PlusOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";
import { getProjectByIdSelector } from "../../@redux/project/selector";
const tabList = [
  {
    key: "config",
    label: "Config",
  },
  {
    key: "loadBalance",
    label: "Load Balance",
  },
];

function Detail() {
  const { id } = useParams();
  const projectInfo = useSelector((state) =>
    getProjectByIdSelector(state, { id: id })
  );
  const configTable = useSelector((state) => state.projects.configs);
  const loading = useSelector((state) => state.projects.loading);
  const dispatch = useDispatch();
  const [activeTab, setActiveTab] = useState("config");
  const navigate = useNavigate();
  const onEdit = (row) => {
    navigate(`/projects/edit/${row.id}`);
  };
  const onCreateConfig = () => {
    navigate(`/projects/createConfig/${projectInfo.id}`);
  };
  const onTabChange = (key) => {
    setActiveTab(key);
  };
  const onDetailConfig = (row) => {
    navigate(`/projects/configDetail/${row}`);
  };
  useEffect(() => {
    dispatch(getProjectById(id));
  }, []);
  const configColumns = [
    {
      title: "name",
      dataIndex: "name",
      key: "name",
      render: (record) => {
        return <Typography className="configName">{record}</Typography>;
      },
    },
    {
      title: "availability zone",
      dataIndex: "availabilityZone",
      key: "availabilityZone",
    },
    {
      title: "current instance",
      dataIndex: "minInstant",
      key: "minInstant",
    },
    {
      title: "status",
      dataIndex: "isActive",
      key: "isActive",
      render: (record) => {
        if (record === true) {
          return <Tag color="green">Active</Tag>;
        }
        return <Tag color="default">Inactive</Tag>;
      },
    },
    {
      title: "private port",
      dataIndex: "privatePort",
      key: "privatePort",
    },
    {
      title: "",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button
            className="editConfig"
            onClick={() => {
              onDetailConfig(record.id);
            }}
          >
            <svg
              width="13"
              height="14"
              viewBox="0 0 13 14"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M11.959 1.18359C11.4004 0.625 10.5117 0.625 9.95312 1.18359L9.19141 1.94531L11.6797 4.43359L12.4414 3.67188C13 3.11328 13 2.22461 12.4414 1.66602L11.959 1.18359ZM4.36719 6.76953C4.21484 6.92188 4.08789 7.125 4.01172 7.32812L3.27539 9.58789C3.19922 9.79102 3.25 10.0449 3.42773 10.1973C3.58008 10.375 3.83398 10.4258 4.03711 10.3496L6.29688 9.61328C6.5 9.53711 6.70312 9.41016 6.85547 9.25781L11.0957 5.01758L8.60742 2.5293L4.36719 6.76953ZM2.4375 2.25C1.0918 2.25 0 3.3418 0 4.6875V11.1875C0 12.5332 1.0918 13.625 2.4375 13.625H8.9375C10.2832 13.625 11.375 12.5332 11.375 11.1875V8.75C11.375 8.31836 10.9941 7.9375 10.5625 7.9375C10.1055 7.9375 9.75 8.31836 9.75 8.75V11.1875C9.75 11.6445 9.36914 12 8.9375 12H2.4375C1.98047 12 1.625 11.6445 1.625 11.1875V4.6875C1.625 4.25586 1.98047 3.875 2.4375 3.875H4.875C5.30664 3.875 5.6875 3.51953 5.6875 3.0625C5.6875 2.63086 5.30664 2.25 4.875 2.25H2.4375Z"
                fill="#6852D3"
              />
            </svg>
          </Button>
        </Space>
      ),
      width: "5%",
    },
  ];
  const contentList = {
    config: (
      <Table
        className="configTable"
        dataSource={configTable}
        pagination={false}
        columns={configColumns}
        loading={loading}
      />
    ),
  };

  return (
    <>
      <HeaderBar
        className="cardTitle"
        display
        search
        title={`Project List / ${projectInfo?.projectName}`}
      />
      <DetailWrapper>
        <Card title="Project info">
          <Row className="titleWrapper">
            <Col span={6}>
              <p>name</p>
            </Col>
            <Col span={6}>
              <p>status</p>
            </Col>
            <Col span={6}>
              <p>region</p>
            </Col>
            <Col span={6}>
              <p>api key</p>
            </Col>
          </Row>
          <Row className="contentWrapper">
            <Col className="imgContainer" span={6}>
              <div>
                <img className="img" src={projectInfo?.image} />
              </div>
              <p className="projectName">{projectInfo?.projectName}</p>
            </Col>
            <Col span={6}>
              <Tag color="green">Active</Tag>
            </Col>
            <Col span={6}>
              <p>{projectInfo?.region}</p>
            </Col>
            <Col span={4}>
              <p>{projectInfo?.apiKey}</p>
            </Col>
            <Col className="buttonContainer" span={2}>
              <Button onClick={() => onEdit(projectInfo)}>
                <svg
                  width="13"
                  height="14"
                  viewBox="0 0 13 14"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M11.959 1.18359C11.4004 0.625 10.5117 0.625 9.95312 1.18359L9.19141 1.94531L11.6797 4.43359L12.4414 3.67188C13 3.11328 13 2.22461 12.4414 1.66602L11.959 1.18359ZM4.36719 6.76953C4.21484 6.92188 4.08789 7.125 4.01172 7.32812L3.27539 9.58789C3.19922 9.79102 3.25 10.0449 3.42773 10.1973C3.58008 10.375 3.83398 10.4258 4.03711 10.3496L6.29688 9.61328C6.5 9.53711 6.70312 9.41016 6.85547 9.25781L11.0957 5.01758L8.60742 2.5293L4.36719 6.76953ZM2.4375 2.25C1.0918 2.25 0 3.3418 0 4.6875V11.1875C0 12.5332 1.0918 13.625 2.4375 13.625H8.9375C10.2832 13.625 11.375 12.5332 11.375 11.1875V8.75C11.375 8.31836 10.9941 7.9375 10.5625 7.9375C10.1055 7.9375 9.75 8.31836 9.75 8.75V11.1875C9.75 11.6445 9.36914 12 8.9375 12H2.4375C1.98047 12 1.625 11.6445 1.625 11.1875V4.6875C1.625 4.25586 1.98047 3.875 2.4375 3.875H4.875C5.30664 3.875 5.6875 3.51953 5.6875 3.0625C5.6875 2.63086 5.30664 2.25 4.875 2.25H2.4375Z"
                    fill="#6852D3"
                  />
                </svg>
              </Button>
            </Col>
          </Row>
        </Card>
        <Card
          className="mainSection"
          tabList={tabList}
          onTabChange={onTabChange}
          tabBarExtraContent={
            <Button
              className="btnNew"
              onClick={() => {
                onCreateConfig();
              }}
            >
              <PlusOutlined />
              New
            </Button>
          }
        >
          {contentList[activeTab]}
        </Card>
      </DetailWrapper>
    </>
  );
}

export default Detail;

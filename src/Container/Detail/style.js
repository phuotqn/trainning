import styled from "styled-components";

const DetailWrapper = styled.div`
  padding: 20px;
  gap: 16px;
  .ant-tabs-nav-wrap {
    padding-left: 15px;
  }
  .cardTitle {
    padding-left: 15px;
  }
  .ant-card .ant-card-body {
    padding: 0px;
  }
  .projectName {
    color: var(--primary-color);
  }
  .ant-table-thead .ant-table-cell {
    font-size: 12px;
    font-style: normal;
    font-weight: 600;
    line-height: normal;
    text-transform: uppercase;
    color: var(--input-icolor);
    background-color: var(--bg-color);
    border: 0px;
  }
  .titleWrapper {
    padding-top: 10px;
    padding-left: 35px;
    padding-right: 35px;
    p {
      text-transform: uppercase;
      font-size: 12px;
      font-style: normal;
      font-weight: 600;
      line-height: normal;
      color: var(--input-icolor);
    }
  }
  .contentWrapper {
    padding-top: 10px;
    padding-left: 35px;
    padding-right: 35px;
    align-items: center;
    padding-bottom: 30px;
    .imgContainer {
      display: flex;
      align-items: center;
      gap: 5px;
    }
    .ant-tag {
      margin-bottom: 5px;
    }
    .img {
      width: 40px;
      height: 40px;
      object-fit: contain;
    }
    .buttonContainer {
      display: flex;
      justify-content: center;
      .ant-btn {
        border: none;
      }
    }
  }
  .mainSection {
    margin-top: 16px;
    .btnNew {
      border-radius: 7px;
      background-color: var(--primary-color);
      color: var(--bg-color);
    }
    .ant-tabs-tab-btn {
      font-size: 14px;
      font-weight: 590;
      color: var(--inactive-tab);
    }
    .configTable {
      padding-left: 10px;
      .configName {
        font-size: 13px;
        font-style: normal;
        font-weight: 400;
        color: var(--primary-color);
      }
      .editConfig {
        border: 0px;
      }
    }
  }
`;
export default DetailWrapper;

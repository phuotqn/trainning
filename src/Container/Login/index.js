import { Button, Form, Input } from "antd";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../../@redux/auth/action";
const VALIDATE = {
  username: [
    {
      message: "Please input valid E-mail",
      type: "email",
    },
    {
      required: true,
      message: "Please input E-mail",
    },
  ],
  password: [
    {
      required: true,
      message: "Please input Password",
    },
  ],
};
const Login = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const loading = useSelector((state) => state.auth.loading);
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  const [form] = Form.useForm();
  const onLogin = async (values) => {
    const { payload } = await dispatch(login(values));
    if (payload.data.accessToken) {
      navigate("/bundle");
    }
  };

  return (
    <div>
      <Form form={form} onFinish={onLogin}>
        <Form.Item name="email" rules={VALIDATE.username} hasFeedback>
          <Input className="loginInput" placeholder="E-mail" />
        </Form.Item>

        <Form.Item name="password" rules={VALIDATE.password}>
          <Input.Password className="loginInput" placeholder="Password" />
        </Form.Item>
        <Form.Item>
          <Button className="loginBtn" htmlType="submit" loading={loading}>
            Sign In
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Login;

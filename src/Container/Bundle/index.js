import { Form, Table, Typography, Tag } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getBundle } from "../../@redux/bundle/action";
import { getAllBundles } from "../../@redux/bundle/selector";
const columns = [
  {
    title: "ID",
    dataIndex: "id",
    width: "30%",
    render: (record) => {
      return (
        <Typography.Link
          style={{ color: "#6852D3", fontSize: "14px" }}
        >{`${record}`}</Typography.Link>
      );
    },
  },
  {
    title: "NAME",
    dataIndex: "name",
    width: "5%",
  },
  {
    title: "SUPPORT",
    dataIndex: "support",
    width: "10%",
  },
  {
    title: "PRICE",
    dataIndex: "price",
    width: "5%",
  },
  {
    title: "POWER",
    dataIndex: "power",
    width: "10%",
  },
  {
    title: "TYPE",
    dataIndex: "type",
    width: "10%",
  },
  {
    title: "CPU",
    dataIndex: "cpu",
    width: "5%",
  },
  {
    title: "RAM (GB)",
    dataIndex: "ram",
    width: "10%",
  },
  {
    title: "MONTHLY TRANSFER (GB)",
    dataIndex: "monthlyTransfer",
    width: "15%",
  },
];

const Bundle = () => {
  const dataSource = useSelector(getAllBundles);
  const loading = useSelector((state) => state.bundles.loading);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getBundle());
  }, []);

  return (
    <Form component={false}>
      <Table
        loading={loading}
        bordered
        dataSource={dataSource}
        columns={columns}
        pagination={true}
      />
    </Form>
  );
};
export default Bundle;

import styled from "styled-components";

const Wrapper = styled.div`
  padding: 20px;
  label {
    color: var(--menu-color);
    font-size: 14px;
    font-style: normal;
  }
  .ant-input {
    width: 235px;
    height: 35px;
    border-radius: 10px;
    border: 1px solid var(--border-color);
  }
  .input {
    width: 235px;
    height: 35px;
    border-radius: 10px;
    border: 1px solid var(--border-color);
  }

  .ant-input-affix-wrapper {
    width: 235px;
    height: 35px;
    border-radius: 10px;
    border: 1px solid var(--border-color);
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .inputNumber {
    width: 235px;
    height: 35px;
    background-color: var(--border-color);
    border-radius: 10px;
    border: 1px solid var(--border-color);
  }
  .ant-form-item {
    color: var(--input-icolor);
    font-size: 14px;
    font-style: normal;
    font-weight: 590;
  }
  .rightSection {
    display: flex;
    flex-direction: column;
    margin-left: 25px;
    align-items: end;
    .input {
      width: 235px;
      height: 35px;
      border-radius: 10px;
      border: 1px solid var(--border-color);
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
  .leftSection {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
  .bundleSection {
    margin-top: 20px;
    color: var(--menu-color);
    font-size: 14px;
    font-style: normal;
    font-weight: 600;
    display: flex;
    flex-direction: column;
    justify-content: center;
    .radio {
      margin-top: 23px;
    }
    .titleWrapper {
      display: flex;
      justify-content: center;
      .listTitle:nth-child(1) {
        display: flex;
        justify-content: center;
        align-items: end;
      }
    }
    .bundleItem {
      padding: 10px;
      display: flex;
      margin: 20px 0px;
      border: 2px solid var(--border-color);
      border-radius: 10px;
      align-items: center;
      justify-content: center;
      p {
        color: var(--menu-color);
        font-size: 14px;
        font-weight: 400;
      }
      .nameCol {
        display: flex;
        gap: 10px;
        align-items: center;
        .name {
          font-size: 14px;
          font-style: normal;
          font-weight: 700;
          color: var(--primary-color);
        }
      }
      .pricingCol {
        padding-left: 24px;
      }
      .supportCol {
        padding-left: 24px;
      }
      .cpuCol {
        padding-left: 30px;
      }
      .diskCol {
        padding-left: 30px;
      }
      .powerCol {
        padding-left: 30px;
      }
      .rightEnd {
        padding-left: 35px;
      }
      .colAction {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 10px;
      }
      .selectButton {
        display: flex;
        justify-content: center;
        width: 60px;
        height: 30px;
        align-items: center;
        background-color: rgba(104, 82, 211, 0.1);
        border-radius: 5px;
        color: var(--primary-color);
      }
      .selectedButton {
        display: flex;
        justify-content: center;
        width: 60px;
        height: 30px;
        align-items: center;
        background-color: var(--primary-color);
        border-radius: 5px;
        color: white;
      }
    }
    .ipAddress {
      padding-top: 10px;
    }
    .bundleItemActive {
      padding: 10px;
      display: flex;
      margin: 20px 0px;
      border: 2px solid var(--primary-color);
      border-radius: 10px;
      align-items: center;
      p {
        color: var(--menu-color);
        font-size: 14px;
        font-weight: 400;
      }
      .nameCol {
        display: flex;
        gap: 10px;
        align-items: center;
        .name {
          font-size: 14px;
          font-style: normal;
          font-weight: 700;
          color: var(--primary-color);
        }
      }
      .pricingCol {
        padding-left: 24px;
      }
      .supportCol {
        padding-left: 24px;
      }
      .cpuCol {
        padding-left: 30px;
      }
      .diskCol {
        padding-left: 30px;
      }
      .powerCol {
        padding-left: 30px;
      }
      .rightEnd {
        padding-left: 35px;
      }
      .colAction {
        display: flex;
        justify-content: center;
        align-items: center;
        margin-top: 10px;
        margin-left: 20px;
      }

      .selectButton {
        display: flex;
        justify-content: center;
        width: 60px;
        height: 30px;
        align-items: center;
        background-color: rgba(104, 82, 211, 0.1);
        border-radius: 5px;
        color: var(--primary-color);
      }
      .selectedButton {
        display: flex;
        justify-content: center;
        width: 60px;
        height: 30px;
        align-items: center;
        background-color: var(--primary-color);
        border-radius: 5px;
        color: white;
      }
    }
  }
`;

export default Wrapper;

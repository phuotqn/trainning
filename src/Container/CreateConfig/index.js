import Wrapper from "./style";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { SaveOutlined, SearchOutlined, CheckOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import {
  createConfig,
  getAvailabilityZone,
  getGitBranches,
  getGitProjects,
  getGitVariables,
  getLoadBalance,
} from "../../@redux/instances/action";
import { getAllBundles } from "../../@redux/bundle/selector";
import { getProjectByIdSelector } from "../../@redux/project/selector";
import {
  getZones,
  getBalances,
  getGitProject,
  getGitBranch,
  getGitVariable,
} from "../../@redux/instances/selector";
import {
  Card,
  Row,
  Col,
  Form,
  Input,
  InputNumber,
  Radio,
  Select,
  Button,
} from "antd";
import HeaderBar from "../Header";
import { getProjectById } from "../../@redux/project/action";
import project from "../../api/projectListApis";
const { Option } = Select;
const listTitle = [
  "Pricing",
  "Supported Platforms",
  "CPU Count",
  "Disk (GB)",
  "Power",
  "RAM (GB)",
  "Monthly transfer (GB)",
];
const VALIDATE = {
  name: [
    {
      required: true,
      message: "Please provide Name",
    },
  ],
  availabilityZone: [
    {
      required: true,
      message: "Please provide Zone",
    },
  ],
  sshKey: [
    {
      required: true,
      message: "Please provide SSH Key",
    },
  ],
  privatePort: [
    {
      required: true,
      message: "Please provide Port",
    },
  ],
  loadBalanceId: [
    {
      required: true,
      message: "Please provide Balance Id",
    },
  ],
  gitProjectId: [
    {
      required: true,
      message: "Please provide Git project ID",
    },
  ],
  gitBranchName: [
    {
      required: true,
      message: "Please provide Branch",
    },
  ],
  gitVariableEnvFile: [
    {
      required: true,
      message: "Please provide Variables",
    },
  ],
};
const initialValues = {
  isActive: true,
};
const CreateConfig = () => {
  const { id } = useParams();
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const bundles = useSelector(getAllBundles);
  const projectInfo = useSelector((state) =>
    getProjectByIdSelector(state, { id: id })
  );
  const zones = useSelector(getZones);
  const balances = useSelector(getBalances);
  const gitProjects = useSelector(getGitProject);
  const gitBranches = useSelector(getGitBranch);
  const gitVariables = useSelector(getGitVariable);
  const [checked, setChecked] = useState(null);
  const buttons = [
    {
      label: "Cancel",
      className: "btnCancel",
    },
    {
      label: "Save",
      className: "btnSave",
      Icon: <SaveOutlined />,
      onClick: () => {
        form.submit();
      },
    },
  ];
  const handleClick = (e) => {
    setChecked(e);
  };
  const onChangeGitProject = (value) => {
    dispatch(getGitBranches(value));
    dispatch(getGitVariables(value));
  };
  const onSave = async (values) => {
    const dataToSend = {
      ...values,
      projectId: id,
    };
    const response = await dispatch(createConfig(dataToSend));
    if (response) {
      form.resetFields();
    }
  };
  useEffect(() => {
    dispatch(getAvailabilityZone(projectInfo.region));
    dispatch(getLoadBalance(projectInfo.id));
    dispatch(getGitProjects());
  }, [projectInfo.id]);

  return (
    <>
      <HeaderBar
        display
        buttons={buttons}
        title={`${projectInfo.projectName} / New Config`}
      />
      <Wrapper>
        <Form
          onFinish={onSave}
          layout="vertical"
          form={form}
          initialValues={initialValues}
        >
          <Card title="General information">
            <Row>
              <Col className="leftSection" span={10}>
                <Form.Item rules={VALIDATE.name} label="Name" name="name">
                  <Input />
                </Form.Item>
                <Form.Item
                  label="Availability zone"
                  name="availabilityZone"
                  rules={VALIDATE.availabilityZone}
                >
                  <Select className="input">
                    {zones.map((zone, i) => {
                      return (
                        <Select.Option key={i} value={zone.zoneName}>
                          {zone.zoneName}
                        </Select.Option>
                      );
                    })}
                  </Select>
                </Form.Item>
                <Form.Item label="Min instance" name="minInstant">
                  <InputNumber className="inputNumber" />
                </Form.Item>
                <Form.Item
                  rules={VALIDATE.sshKey}
                  label="SSH key"
                  name="sshKey"
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col span={6}>
                {" "}
                <Form.Item
                  label="Status"
                  className="radioGroup"
                  name="isActive"
                >
                  <Radio.Group>
                    <Radio value={true}>Active</Radio>
                    <Radio value={false}>Inactive</Radio>
                  </Radio.Group>
                </Form.Item>
                <Form.Item
                  rules={VALIDATE.privatePort}
                  label="Private port"
                  name="privatePort"
                >
                  <Input />
                </Form.Item>
                <Form.Item
                  rules={VALIDATE.loadBalanceId}
                  name="loadBalanceId"
                  label="Load balance"
                >
                  <Select className="input">
                    {balances ? (
                      balances.map((balance) => {
                        return (
                          <Option value={balance.id}>{balance.name}</Option>
                        );
                      })
                    ) : (
                      <Option />
                    )}
                  </Select>
                </Form.Item>
                <Form.Item
                  className="ipAddress"
                  label="IP Address"
                  name="ipAddressType"
                >
                  <Radio.Group>
                    <Radio value="ipv4">IPv4</Radio>
                    <Radio value="ipv6">IPv6</Radio>
                  </Radio.Group>
                </Form.Item>
              </Col>
              <Col className="rightSection" span={6}>
                <Form.Item
                  rules={VALIDATE.gitProjectId}
                  label="Git project"
                  name="gitProjectId"
                >
                  <Select
                    className="input"
                    showSearch
                    suffixIcon={<SearchOutlined />}
                    onChange={onChangeGitProject}
                  >
                    {gitProjects &&
                      gitProjects.map((gitProject) => (
                        <Option key={gitProject.id} value={gitProject.id}>
                          {gitProject.name}
                        </Option>
                      ))}
                  </Select>
                </Form.Item>
                <Form.Item
                  rules={VALIDATE.gitBranchName}
                  label="Git branch"
                  name="gitBranchName"
                >
                  <Select
                    className="input"
                    showSearch
                    suffixIcon={<SearchOutlined />}
                  >
                    {gitBranches &&
                      gitBranches.map((branch) => {
                        return (
                          <Option value={branch.name} key={branch.name}>
                            {branch.name}
                          </Option>
                        );
                      })}
                  </Select>
                </Form.Item>
                <Form.Item
                  rules={VALIDATE.gitVariableEnvFile}
                  name="gitVariableEnvFile"
                  label="Git variable"
                >
                  <Select className="input">
                    {gitVariables &&
                      gitVariables.map((variable) => {
                        return (
                          <Option
                            value={variable.variable_type}
                            key={variable.key}
                          >
                            {variable.variable_type}
                          </Option>
                        );
                      })}
                  </Select>
                </Form.Item>
              </Col>
            </Row>
          </Card>
          <Card className="bundleSection" title="Bundle">
            <Row className="titleWrapper">
              {listTitle.map((list) => {
                return (
                  <Col className="listTitle" span={3}>
                    {list}
                  </Col>
                );
              })}
            </Row>
            {bundles.map((item, key) => {
              return (
                <Row
                  key={item.id}
                  className={
                    checked === item.id ? "bundleItemActive" : "bundleItem"
                  }
                >
                  <Col span={2} className="nameCol">
                    <Form.Item key={item.id} name="bundleId">
                      <Radio
                        key={item.id}
                        onChange={() => handleClick(item.id)}
                        value={item.id}
                        checked={checked === item.id}
                        className="radio"
                      />
                    </Form.Item>
                    <p className="name">{item.name}</p>
                  </Col>
                  <Col className="pricingCol" span={2}>
                    <p>${item.price}</p>
                  </Col>
                  <Col className="supportCol" span={3}>
                    <p>{item.support}</p>
                  </Col>
                  <Col className="cpuCol" span={3}>
                    <p>{item.cpu}</p>
                  </Col>
                  <Col className="diskCol" span={3}>
                    <p>{item.code}</p>
                  </Col>
                  <Col className="powerCol" span={3}>
                    <p>{item.power}</p>
                  </Col>
                  <Col className="rightEnd" span={3}>
                    <p>{item.ram}</p>
                  </Col>
                  <Col className="rightEnd" span={3}>
                    <p>{item.monthlyTransfer}</p>
                  </Col>
                  <Form.Item name="bundleId" className="colAction">
                    <Col span={2}>
                      <Button
                        className={
                          checked === item.id
                            ? "selectedButton"
                            : "selectButton"
                        }
                        key={item.id}
                        onClick={() => handleClick(item.id)}
                      >
                        {checked === item.id ? <CheckOutlined /> : "Select"}
                      </Button>
                    </Col>
                  </Form.Item>
                </Row>
              );
            })}
          </Card>
        </Form>
      </Wrapper>
    </>
  );
};
export default CreateConfig;

import { createGlobalStyle } from "styled-components";
export const OverrideAnt = createGlobalStyle`
    .contentWrapper {
        display: flex;
        align-items: center;
        padding-top: 10px;
    }
    .defaultRad {
        margin-bottom: 10px;
    }
    .customRad {
        margin-top: 5px;
    }
 .btnSave {
      width: 85px;
      height: 30px;
      background-color: var(--primary-color);
      font-size: 13px;
      font-weight: 500px;
        color: var(--bg-color)
    }
    .btnCancel {
        background-color: var(--bg-color);
        font-size: 13px;
        width: 85px;
      height: 30px;
      color: var(--primary-color)
    }
    .inputRow {
        padding-left: 10px;
        display: flex;
        flex-direction: column;
        gap: 5px;
        .ant-input-disabled {
            width: 325px;
            color: var(--input-newInstance);
            font-weight: 400;
            font-size: 13px;
            border-radius: 10px;
        }
        .ant-select {
            width: 325px;
            border-radius: 10px;
        }
    }
`;

const theme = {
  token: {
    colorPrimary: "#6852D3",
    fontFamily: "SF Pro Text",
  },
  components: {
    Input: {
      colorBgContainer: "#F6F7F9",
    },
    Menu: {
      itemSelectedColor: "#6852D3",
      itemHoverColor: "#6852D3",
    },
    Button: {
      backgroundColor: "#6852D3",
    },
    Select: {
      colorBgContainer: "#F6F7F9",
    },
  },
};

export default theme;

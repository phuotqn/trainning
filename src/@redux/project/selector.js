import { createSelector } from "reselect";

const projects = (state) => state.projects.data;
const ids = (state) => state.projects.ids;

export const getProjectList = createSelector(
  [projects, ids],
  (data = {}, ids = []) => {
    return ids.map((id) => data[id]);
  }
);

export const getProjectByIdSelector = createSelector(
  [projects, (_, props) => props.id],
  (data, id) => data[id]
);

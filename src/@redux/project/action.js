import { createAsyncThunk } from "@reduxjs/toolkit";
import project from "../../api/projectListApis";
import { notification } from "antd";

export const getProjectLists = createAsyncThunk(
  "projects/getProjects",
  async (rejectWithValue) => {
    try {
      const response = await project.getList();
      return response.data.results || [];
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to retrieve Project List",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue("Error");
      }
    }
  }
);

export const getProjectById = createAsyncThunk(
  "projects/getProjectById",
  async (id, rejectWithValue) => {
    try {
      const response = await project.getProjectById(id);
      localStorage.setItem("apiKey", response.data.apiKey);
      return response.data;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to retrieve Project",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue("Error");
      }
    }
  }
);

export const createProject = createAsyncThunk(
  "projects/createProject",
  async (body, { rejectWithValue }) => {
    try {
      const response = await project.createProject(body);
      if (response) {
        notification.success({
          message: "Project Created Successfully",
        });
      }
      return response.data;
    } catch (err) {
      if (err) {
        notification.error({
          message: "Failed to create Project",
        });
        return rejectWithValue(err);
      } else {
        return rejectWithValue("Error");
      }
    }
  }
);
export const editProject = createAsyncThunk(
  "projects/editProject",
  async ({ id, body }, rejectWithValue) => {
    try {
      const response = await project.editProject(id, body);
      if (response) {
        notification.success({
          message: "Project edited Successfully",
        });
      }
      return response.data;
    } catch (err) {
      if (err) {
        notification.error({
          message: "Failed to edit Project",
        });
        return rejectWithValue(err);
      } else {
        return rejectWithValue("Error");
      }
    }
  }
);
export const deleteProject = createAsyncThunk(
  "projects/deleteProject",
  async (id, rejectWithValue) => {
    try {
      const response = await project.deleteProject(id);
      notification.success({
        message: "Delete project successfully",
      });
      return {
        ...response,
        id,
      };
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to delete",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue("Error");
      }
    }
  }
);

import {
  getProjectLists,
  getProjectById,
  createProject,
  editProject,
  deleteProject,
} from "./action";
import _ from "lodash";
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: {},
  ids: [],
  configs: [],
  projectInfo: [],
  currentProject: null,
  loading: false,
};

const { reducer } = createSlice({
  initialState,
  name: "projects",
  reducers: {
    getListSuccess: (state, action) => {
      state.loading = false;
      state.projectList = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getProjectLists.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getProjectLists.fulfilled, (state, action) => {
      const normalizedList = action.payload.reduce(
        (obj, key) => ({
          ...obj,
          [key.id]: key,
        }),
        {}
      );
      state.loading = false;
      state.data = normalizedList;
      state.ids = action.payload.map((e) => e.id);
    });
    builder.addCase(getProjectLists.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(getProjectById.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(getProjectById.fulfilled, (state, action) => {
      state.loading = false;
      state.data = { [action.payload.id]: action.payload };
      state.configs = action.payload.configs.map((config) => config);
    });
    builder.addCase(createProject.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(createProject.fulfilled, (state, action) => {
      state.loading = false;
      state.data = { ...state.data, ...action.payload };
    });
    builder.addCase(createProject.rejected, (state, action) => {
      state.loading = false;
      state.error = action.payload;
    });
    builder.addCase(editProject.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(editProject.fulfilled, (state, action) => {
      state.loading = false;
      state.data = { [action.payload.id]: action.payload };
    });
    builder.addCase(editProject.rejected, (state, action) => {
      state.loading = true;
      state.error = action.payload;
    });
    builder.addCase(deleteProject.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(deleteProject.fulfilled, (state, action) => {
      state.ids = state.ids.filter(
        (projectId) => projectId !== action.payload.id
      );
      state.loading = false;
    });
    builder.addCase(deleteProject.rejected, (state, action) => {
      state.loading = false;
    });
  },
});
export default reducer;

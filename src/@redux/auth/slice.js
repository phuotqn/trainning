import { createSlice } from "@reduxjs/toolkit";
import { login } from "./action";
export const initialState = {
  isAuthenticated: !!localStorage.getItem("token"),
  userData: localStorage.getItem("user") || "",
};

const { reducer } = createSlice({
  name: "login",
  initialState,
  reducers: {
    loginSucess: (state, action) => {
      state.loading = true;
    },
    loginFail: (state, action) => {
      state.loading = true;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(login.pending, (state) => {
      state.isAuthenticated = false;
      state.loading = true;
    });
    builder.addCase(login.fulfilled, (state) => {
      state.isAuthenticated = true;
      state.loading = false;
    });
    builder.addCase(login.rejected, (state) => {
      state.isAuthenticated = false;
      state.loading = false;
    });
  },
});

export default reducer;

import { createAsyncThunk } from "@reduxjs/toolkit";
import loginApi from "../../api/loginApis";
import { notification } from "antd";
export const login = createAsyncThunk(
  "auth/login",
  async ({ email, password }, { rejectWithValue }) => {
    try {
      const response = await loginApi.login(email, password);
      if (response.data.accessToken) {
        localStorage.setItem("token", response.data.accessToken);
        localStorage.setItem("user", response.config.data);
      }
      return response;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Login failed",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue("Error");
      }
    }
  }
);

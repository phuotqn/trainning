import { createSelector } from "reselect";

const bundles = (state) => state.bundles.data;

export const getAllBundles = createSelector([bundles], (data = {}) => {
  return Object.values(data);
});

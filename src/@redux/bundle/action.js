import bundle from "../../api/getBundlesApis";
import { notification } from "antd";
import { createAsyncThunk } from "@reduxjs/toolkit";

export const getBundle = createAsyncThunk(
  "bundle/getBundles",
  async (rejectWithValue) => {
    try {
      const response = await bundle.getBundle();
      return response.data.results;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to retrieve Bundle",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue("Error");
      }
    }
  }
);

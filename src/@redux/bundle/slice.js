import { createSlice } from "@reduxjs/toolkit";
import { getBundle } from "./action";
import _ from "lodash";
const initialState = {
  data: [],
  currentId: null,
  loading: false,
};

const { reducer } = createSlice({
  name: "bundle",
  initialState,
  reducers: {
    getDataSuccess: (state, action) => {
      state.loading = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getBundle.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getBundle.fulfilled, (state, action) => {
      const newData = _.mapKeys(action.payload, "id");
      state.loading = false;
      state.data = { ...state.data, ...newData };
    });

    builder.addCase(getBundle.rejected, (state) => {
      state.loading = false;
    });
  },
});

export default reducer;

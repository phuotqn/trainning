import instance from "../../api/getInstancesApis";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { notification } from "antd";
export const getInstances = createAsyncThunk(
  "instances/getInstances",
  async (rejectWithValue) => {
    try {
      const response = await instance.getRegion();
      return response.data.results;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to retrieve Regions",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue("Error");
      }
    }
  }
);
export const getAvailabilityZone = createAsyncThunk(
  "instances/getAvailabilityZone",
  async (code, rejectWithValue) => {
    try {
      const response = await instance.getAvailabilityZone(code);
      return response.data.results;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to retrieve Availability Zone",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue("Error");
      }
    }
  }
);
export const getLoadBalance = createAsyncThunk(
  "instances/getLoadBalance",
  async (code, rejectWithValue) => {
    try {
      const response = await instance.getLoadBalance(code);
      return response.data.results;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to retrieve Balance",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue("Error");
      }
    }
  }
);
export const getGitProjects = createAsyncThunk(
  "instances/getGitProjects",
  async (rejectWithValue) => {
    try {
      const response = await instance.getGitProjects();
      return response.data.results;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to retrieve Git projects",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue(err);
      }
    }
  }
);
export const getGitBranches = createAsyncThunk(
  "instances/getGitBranches",
  async (id, rejectWithValue) => {
    try {
      const response = await instance.getGitBranches(id);
      return response.data.results;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to retrieve Git branches",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue(err);
      }
    }
  }
);
export const getGitVariables = createAsyncThunk(
  "instances/getGitVariables",
  async (id, rejectWithValue) => {
    try {
      const response = await instance.getGitVariables(id);
      return response.data.results;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to retrieve Git variables",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue(err);
      }
    }
  }
);
export const createConfig = createAsyncThunk(
  "instances/createConfig",
  async (body, rejectWithValue) => {
    try {
      const response = await instance.createConfig(body);
      if (response) {
        notification.success({
          message: "Config Created Successfully",
        });
      }
      return response.data;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to create Config",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue(err);
      }
    }
  }
);
export const getConfigDetail = createAsyncThunk(
  "instances/getConfigDetail",
  async (id, rejectWithValue) => {
    try {
      const response = await instance.getConfigDetail(id);
      return response.data;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to get Config Detail",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue(err);
      }
    }
  }
);
export const getConfigInstances = createAsyncThunk(
  "instances/getConfigInstances",
  async ({ page, limit, filter, configId }, rejectWithValue) => {
    try {
      const response = await instance.getConfigInstances(
        page,
        limit,
        filter,
        configId
      );
      return response.data;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to get Config Instances",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue(err);
      }
    }
  }
);
export const createInstance = createAsyncThunk(
  "instances/createInstance",
  async (body, rejectWithValue) => {
    try {
      const response = await instance.createConfigInstance(body);
      if (response) {
        notification.success({
          message: "Instance created successfully",
        });
        return response.data;
      }
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to create instance",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue(err);
      }
    }
  }
);
export const deleteInstance = createAsyncThunk(
  "instances/deleteInstance",
  async (body, rejectWithValue) => {
    try {
      const response = await instance.deleteConfigInstance(body);
      if (response) {
        notification.success({
          message: "Config Deleted Successfully",
        });
        return {
          ...response,
          body,
        };
      }
      return body;
    } catch (err) {
      if (err.response) {
        notification.error({
          message: "Failed to delete Config Instances",
        });
        return rejectWithValue(err.response);
      } else {
        return rejectWithValue(err);
      }
    }
  }
);

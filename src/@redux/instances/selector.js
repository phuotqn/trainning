import { createSelector } from "reselect";

const zones = (state) => state.instances.zones;
const balances = (state) => state.instances.balances;
const gitProjects = (state) => state.instances.projects;
const gitBranches = (state) => state.instances.branches;
const gitVariables = (state) => state.instances.variables;
const configs = (state) => state.instances.configs;
const ids = (state) => state.instances.allConfigIds;
const total = (state) => state.instances.total;
export const getZones = createSelector([zones], (zones = []) => {
  return zones;
});
export const getBalances = createSelector([balances], (balances = []) => {
  return balances;
});
export const getGitProject = createSelector(
  [gitProjects],
  (gitProjects = []) => {
    return gitProjects;
  }
);
export const getGitBranch = createSelector(
  [gitBranches],
  (gitBranches = []) => {
    return gitBranches;
  }
);
export const getGitVariable = createSelector(
  [gitVariables],
  (gitVariables = []) => {
    return gitVariables;
  }
);
export const getTotal = createSelector([total], (total = 0) => {
  return total;
});
export const getInstances = createSelector(
  [configs, ids],
  (data = {}, ids = []) => {
    return ids.map((id) => data[id]);
  }
);

export const getConfigInstanceByIdSelector = createSelector(
  [configs, (_, props) => props.id],
  (data, id) => data[id]
);

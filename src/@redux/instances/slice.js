import {
  getInstances,
  getAvailabilityZone,
  getLoadBalance,
  getGitProjects,
  getGitBranches,
  getGitVariables,
  createConfig,
  getConfigInstances,
  getConfigDetail,
  deleteInstance,
  createInstance,
} from "./action";
import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  regions: [],
  zones: [],
  configs: {},
  allConfigIds: [],
  total: 0,
};

const { reducer } = createSlice({
  initialState,
  name: "instances",
  reducers: {
    success: (state) => {
      state.loading = false;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getInstances.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getInstances.fulfilled, (state, action) => {
      state.regions = action.payload;
      state.loading = false;
    });
    builder.addCase(getInstances.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(getAvailabilityZone.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getAvailabilityZone.fulfilled, (state, action) => {
      state.loading = false;
      state.zones = action.payload;
    });
    builder.addCase(getAvailabilityZone.rejected, (state) => {
      state.loading = false;
      state.error = true;
    });
    builder.addCase(getLoadBalance.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getLoadBalance.fulfilled, (state, action) => {
      state.loading = false;
      state.balances = action.payload;
    });
    builder.addCase(getLoadBalance.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(getGitProjects.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getGitProjects.fulfilled, (state, action) => {
      state.loading = false;
      state.projects = action.payload;
    });
    builder.addCase(getGitProjects.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(getGitBranches.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getGitBranches.fulfilled, (state, action) => {
      state.loading = false;
      state.branches = action.payload;
    });
    builder.addCase(getGitBranches.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(getGitVariables.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getGitVariables.fulfilled, (state, action) => {
      state.loading = false;
      state.variables = action.payload;
    });
    builder.addCase(getGitVariables.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(createConfig.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(createConfig.fulfilled, (state, action) => {
      state.loading = false;
      state.newConfig = action.payload;
    });
    builder.addCase(createConfig.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(getConfigDetail.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getConfigDetail.fulfilled, (state, action) => {
      state.loading = false;
      state.configDetail = action.payload;
    });
    builder.addCase(getConfigDetail.rejected, (state) => {
      state.loading = false;
    });
    builder.addCase(getConfigInstances.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getConfigInstances.fulfilled, (state, action) => {
      const normalizedList = action.payload.results.reduce(
        (obj, key) => ({
          ...obj,
          [key.id]: key,
        }),
        {}
      );
      state.loading = false;
      state.configs = normalizedList;
      state.allConfigIds = action.payload.results.map((e) => e.id);
      state.total = action.payload.total;
    });
    builder.addCase(getConfigInstances.rejected, (state, action) => {
      state.loading = false;
    });
    builder.addCase(deleteInstance.pending, (state) => {
      state.isUpdate = false;
    });
    builder.addCase(deleteInstance.fulfilled, (state, action) => {
      state.loading = false;
      state.allConfigIds = state.allConfigIds.filter(
        (configId) => configId !== action.payload.body.instanceId
      );
      state.total -= 1;
    });
    builder.addCase(deleteInstance.rejected, (state) => {
      state.isUpdate = false;
    });
    builder.addCase(createInstance.pending, (state) => {
      state.isUpdate = false;
    });
    builder.addCase(createInstance.fulfilled, (state, action) => {
      state.isUpdate = true;
      state.total += action.payload.name.length;
      console.log(action.payload);
    });
    builder.addCase(createInstance.rejected, (state) => {
      state.isUpdate = false;
    });
  },
});
export default reducer;

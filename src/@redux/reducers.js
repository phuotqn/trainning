import { combineReducers } from "@reduxjs/toolkit";

import auth from "./auth/slice";
import bundles from "./bundle/slice";
import projects from "./project/slice";
import instances from "./instances/slice";
export default () =>
  combineReducers({
    auth,
    bundles,
    projects,
    instances,
  });

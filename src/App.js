import "./App.css";
import "../src/styles/MainGradient.css";
import { ConfigProvider } from "antd";
import BundlePage from "./pages/Bundle";
import LoginPage from "./pages/Login/index";
import ProjectPage from "./pages/List/index";
import DetailPage from "./pages/Detail";
import CreatePage from "./pages/Create";
import CreateConfigPage from "./pages/CreateConfig";
import ConfigDetailPage from "./pages/ConfigDetail";
import EditPage from "./pages/Edit";
import theme from "./styles/theme";
import { GlobalStyles, OverrideAnt } from "./styles/";
import {
  Route,
  createRoutesFromElements,
  RouterProvider,
} from "react-router-dom";
import { useSelector } from "react-redux";
import { createBrowserRouter, useNavigate } from "react-router-dom";
import PrivateLayout from "./Layout/PrivateLayout";
import DetailLayout from "./Layout/DetailLayout";
function App() {
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);

  const router = createBrowserRouter(
    createRoutesFromElements(
      <Route element={isAuthenticated ? <PrivateLayout /> : <LoginPage />}>
        <Route
          path="/"
          element={isAuthenticated ? <BundlePage /> : <LoginPage />}
        />
        <Route path="/bundle" exact element={<BundlePage />} />
        <Route path="/projects" element={<DetailLayout />}>
          <Route path="list" element={<ProjectPage />} />
          <Route path="list/:id" element={<DetailPage />} />
          <Route path="edit/:id" element={<EditPage />} />
          <Route path="create" element={<CreatePage />} />
          <Route path="createConfig/:id" element={<CreateConfigPage />} />
          <Route path="configDetail/:id" element={<ConfigDetailPage />} />
        </Route>
      </Route>
    )
  );

  return (
    <ConfigProvider theme={theme}>
      <GlobalStyles />
      <OverrideAnt />
      <RouterProvider router={router} />
    </ConfigProvider>
  );
}

export default App;

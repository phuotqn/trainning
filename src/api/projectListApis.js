import { axiosClientInstance } from "./axiosClient";

const project = {
  getList: async () => {
    const endPoint = "/projects";
    return await axiosClientInstance.get(endPoint);
  },
  getProjectById: async (id) => {
    const endPoint = `/projects/${id}`;
    return await axiosClientInstance.get(endPoint);
  },
  createProject: async (body) => {
    const endPoint = "/projects";
    return await axiosClientInstance.post(endPoint, body);
  },
  editProject: async (id, body) => {
    const endPoint = `/projects/${id}`;
    return await axiosClientInstance.put(endPoint, body);
  },
  deleteProject: async (id) => {
    const endPoint = `/projects/${id}`;
    return await axiosClientInstance.delete(endPoint);
  }
};
export default project;

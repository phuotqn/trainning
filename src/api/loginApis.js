import { axiosClientInstance } from "./axiosClient";
const loginApi = {
  login: async (email, password) => {
    const endPoint = "/auth/login";
    return await axiosClientInstance.post(endPoint, {
      email: email,
      password: password,
    });
  },
};

export default loginApi;

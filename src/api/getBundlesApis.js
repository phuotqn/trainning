import { axiosClientInstance } from "./axiosClient";

const bundle = {
  getBundle: async () => {
    const endPoint = "/bundles";
    return await axiosClientInstance.get(endPoint);
  },
};

export default bundle;

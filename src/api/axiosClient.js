import axios from "axios";

const URL = "https://shaker-api-dev.devtify.com/api/v1";

const axiosClientInstance = axios.create({
  baseURL: URL,
  headers: {
    "Content-Type": "application/json",
  },
});
axiosClientInstance.interceptors.request.use(function (config) {
  const token = localStorage.getItem("token");
  const apiKey = localStorage.getItem("apiKey");
  config.headers = {
    "Content-Type": "application/json",
    Authorization: `Bearer ${token}`,
    "X-API-KEY": apiKey,
  };
  return config;
});

export { axiosClientInstance };

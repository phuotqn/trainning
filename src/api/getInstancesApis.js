import { axiosClientInstance } from "./axiosClient";

const instance = {
  getRegion: async () => {
    const endPoint = "/resources/regions";
    return await axiosClientInstance.get(endPoint);
  },
  getAvailabilityZone: async (code) => {
    const endPoint = `/resources/regions/${code}/availability-zone`;
    return await axiosClientInstance.get(endPoint);
  },
  getLoadBalance: async () => {
    const endPoint = `/load-balance`;
    return await axiosClientInstance.get(endPoint);
  },
  getGitProjects: async () => {
    const endPoint = `/gitlab/projects`;
    return await axiosClientInstance.get(endPoint);
  },
  getGitBranches: async (id) => {
    const endPoint = `/gitlab/projects/${id}/repository/branches`;
    return await axiosClientInstance.get(endPoint);
  },
  getGitVariables: async (id) => {
    const endPoint = `/gitlab/projects/${id}/variables`;
    return await axiosClientInstance.get(endPoint);
  },
  createConfig: async (body) => {
    const endPoint = "/configs";
    return await axiosClientInstance.post(endPoint, body);
  },
  getConfigDetail: async (id) => {
    const endPoint = `/configs/${id}`;
    return await axiosClientInstance.get(endPoint);
  },
  getConfigInstances: async (page, limit, filter, configId) => {
    const endPoint = `/instance?page=${page}&limit=${limit}&filter={"configId__":"${configId}"}`;
    return await axiosClientInstance.get(endPoint, page, limit, filter);
  },
  deleteConfigInstance: async (id) => {
    const endPoint = `/strategy/scale-down`;
    return await axiosClientInstance.put(endPoint, id);
  },
  createConfigInstance: async (body) => {
    const endPoint = `/strategy/scale-up`;
    return await axiosClientInstance.put(endPoint, body);
  },
};
export default instance;
